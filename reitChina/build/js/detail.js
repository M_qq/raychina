// var e;
// !function(n,e){
// 	function t(){
// 		e.documentElement.style.fontSize=100*e.documentElement.clientWidth/375+"px"
// 	}t(),n.addEventListener("resize",t,!1)}(window,document),
	$(function(){
        
        initData()
        
        $('.but').click(function(){
        	$.ajax({
        		url: hostUrl+"/comments/save",
			    type: "post",
			    data: {
			    	showId:(window.location.search).split('=')[1],
			    	context:$('#comment').val(),
			    },
			    async:true,
			    headers: {
			      'Content-Type': 'application/x-www-form-urlencoded'
			    },
			    success: function(res){
			    	  if(res.returnCode == '0000'){
			    	  	initData()
	                     $('#comment').val('')
			    	  }  
			    }
        	})
        })
    
	});


function initData(){
	$.ajax({
		    url: hostUrl+"/shows/showVOById",
		    type: "post",
		    data: {
		    	id:(window.location.search).split('=')[1],
		    },
		    async:true,
		    headers: {
		      'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(res){
		    	var data = ''
		    	  if(res.returnCode == '0000'){
                      $('.richText').html(res.data.richText)
					  $('.titles').html(res.data.title)
					  $('.timePic').html(res.data.createTime)
					  if(res.data.commentsVOList.length>0){
                      	$('.comment').show()
                          for(var i=0;i<res.data.commentsVOList.length;i++){
                         data+=`  <li>
					         <img src="${res.data.commentsVOList[i].img}" class="left">
					         <div class="right">
					           <div class="top">
					             <div>${res.data.commentsVOList[i].userName}</div>
					             <div>${res.data.commentsVOList[i].createdate}</div>
					           </div>
					           <div class="bottom">${res.data.commentsVOList[i].context}</div>
					         </div>
					       </li>`
                      }
                      $('.list').html(data)
                    }
                      
		    	  }  
		    }
		})
}