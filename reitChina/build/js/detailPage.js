var e;
!function(n,e){
	function t(){
		e.documentElement.style.fontSize=100*e.documentElement.clientWidth/375+"px"
	}t(),n.addEventListener("resize",t,!1)}(window,document),
	$(function(){
         $.ajax({
		    url: hostUrl+"/shows/getAPPPage",
		    type: "post",
		    data: {
		    	type:(window.location.search).split('=')[1],
		    },
		    async:true,
		    headers: {
		      'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(res){
		    	var data = ''
		    	  if(res.returnCode == '0000'){
                     for(var i=0;i<res.data.length;i++){
                     	data+=` <li data-id="${res.data[i].id}">
						      <div class="left">
						        <img src="${res.data[i].coverImg}">
						      </div>
						      <div class="right">
						        <div class="title">${res.data[i].title}</div>
						        <div class="introduce">${res.data[i].context}</div>
						      </div>
						    </li>`
                     }
                     $('#main').children('ul').html(data)
		    	  }  
		    }
		})

         $('#main').on('click','li',function(){
           window.location.href="detail.html?id="+$(this).data('id')
        })
	});

	