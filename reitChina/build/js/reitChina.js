var e;
!function (n, e) {
    function t() {
        e.documentElement.style.fontSize = 100 * e.documentElement.clientWidth / 375 + "px"
    }

    t(), n.addEventListener("resize", t, !1)
}(window, document),
    $(function () {
        $.ajax({
            url: hostUrl + "/showsSynopsis/findById",
            type: "post",
            data: {
                id: 1,
            },
            async: true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            success: function (res) {
                if (res.returnCode == '0000') {
                    $('.mainly').html(res.data.context)
                }
            }
        })
        $.ajax({
            url: hostUrl + "/shows/getAPPPage",
            type: "post",
            data: {
                type: 'RAYFOR',
            },
            async: true,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            success: function (res) {
                var data = ''
                if (res.returnCode == '0000') {
                    for (var i = 0; i < res.data.length; i++) {
                        data += `<div class="slide">
						         <div class="slides">
						           <div class="message">
						                <img src="${res.data[i].expertsImg}" class="headPortrait">
                                        <div class="name">${res.data[i].expertsName}</div>
                                        <div class="introduce">${res.data[i].expertsProfile}</div>
						           </div>
						           <div class="box">
						             <div class="shadow">
						               <div class="pad14">
						                 <div class="title">${res.data[i].title}</div>
						                 <span></span>
						                 <div class="content">${res.data[i].context}</div>
						               </div>
						               <div class="readMore">
						                   <div class="but" onclick = 'void(0)' data-id="${res.data[i].id}">阅读更多</div>
						                   <input type="hidden" value="${res.data[i].id}">
						                 </div>
						             </div>
						           </div>
						         </div>
						       </div>`
                    }
                    // $('#swaper').html(data)
                    //插入指定内容
                    $('#swaper').append(data)
                    //点击事件
                    $('#swaper .but').click(function(){
                        var id = $(this).next('input').val();
                        window.location.href = "detail.html?id="+id;
                    })
                    $('#fullpage').fullpage({
                        'resize': true,
                        'continuousVertical': false,
                        'slidesNavigation': true,
                        'css3': true,
                        observer: true,//修改swiper自己或子元素时，自动初始化swiper
                        observeParents: true,//修改swiper的父元素时，自动初始化swiper
                        touchSensitivity: 8,
                        slidesPerView: 'auto',
                        loopHorizontal: true,
                    })
                    setInterval(function(){
                        $.fn.fullpage.moveSlideRight();
                    }, 3000);
                }
            }
        })
        /*$('body').on('click touchstart', '.but', function () {
            window.location.href = "detail.html?id=" + $(this).data('id')
        })*/
        $('#fullpage').on('click', '.tab', function () {
            window.location.href = "detailPage.html?type=" + $(this).data('type')
        })
    });
