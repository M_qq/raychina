var e;
!function(n,e){
	function t(){
		e.documentElement.style.fontSize=100*e.documentElement.clientWidth/375+"px"
	}t(),n.addEventListener("resize",t,!1)}(window,document),
	$(function(){

        initData('PALLIATIVE')
        
        $('#main').on('click','li',function(){
           window.location.href="detail.html?id="+$(this).data('id')
        })



        $('.tab').click(function(){
        	if($(this).data('type') == 'PALLIATIVE'){
                 $(this).removeClass('tab1')
                 $(this).addClass('img1')
                 $(this).siblings('.tab').removeClass('img2')
                 $(this).siblings('.tab').removeClass('img3')
                 $(this).siblings('.tab').removeClass('img4')
                 $(this).siblings('.tab').addClass('tab2')
                 $(this).siblings('.tab').addClass('tab3')
                 $(this).siblings('.tab').addClass('tab4')
                 initData('PALLIATIVE')
        	}else if($(this).data('type') == 'PERIOPERATIVE'){
                 $(this).removeClass('tab2')
                 $(this).addClass('img2')
                 $(this).siblings('.tab').removeClass('img1')
                 $(this).siblings('.tab').removeClass('img3')
                 $(this).siblings('.tab').removeClass('img4')
                 $(this).siblings('.tab').addClass('tab1')
                 $(this).siblings('.tab').addClass('tab3')
                 $(this).siblings('.tab').addClass('tab4')
                 initData('PERIOPERATIVE')
        	}else if($(this).data('type') == 'INTERVENTIONAL'){
                 $(this).removeClass('tab3')
                 $(this).addClass('img3')
                 $(this).siblings('.tab').removeClass('img1')
                 $(this).siblings('.tab').removeClass('img2')
                 $(this).siblings('.tab').removeClass('img4')
                 $(this).siblings('.tab').addClass('tab1')
                 $(this).siblings('.tab').addClass('tab2')
                 $(this).siblings('.tab').addClass('tab4')
                 initData('INTERVENTIONAL')
        	}else{
                 $(this).removeClass('tab4')
                 $(this).addClass('img4')
                 $(this).siblings('.tab').removeClass('img1')
                 $(this).siblings('.tab').removeClass('img3')
                 $(this).siblings('.tab').removeClass('img2')
                 $(this).siblings('.tab').addClass('tab1')
                 $(this).siblings('.tab').addClass('tab3')
                 $(this).siblings('.tab').addClass('tab2')
                 initData('RADIATION')
        	}
        })

	});

	function initData(type){
		$.ajax({
		    url: hostUrl+"/shows/getAPPPage",
		    type: "post",
		    data: {
		    	type:type,
		    },
		    async:true,
		    headers: {
		      'Content-Type': 'application/x-www-form-urlencoded'
		    },
		    success: function(res){
		    	var data = ''
		    	  if(res.returnCode == '0000'){
                     for(var i=0;i<res.data.length;i++){
                     	data+=` <li data-id="${res.data[i].id}">
						      <div class="left">
						        <img src="${res.data[i].coverImg}">
						      </div>
						      <div class="right">
						        <div class="title">${res.data[i].title}</div>
						        <div class="introduce">${res.data[i].context}</div>
						      </div>
						    </li>`
                     }
                     $('#main').children('ul').html(data)
		    	  }  
		    }
		})
	}